package fr.isika.cdi.si.test;


import static org.junit.Assert.assertEquals;
import org.junit.Test;
import fr.isika.cdi.si.App;
public class AppTestDevOps {
	@Test
	public void testerAfficherMessage() {
		assertEquals("Erreur de retour message", "Hello Linux", App.afficherMessage("Hello Linux"));
	}
}
